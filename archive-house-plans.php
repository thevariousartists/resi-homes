<h1 class="col-12">Plans</h1>

<?php if(have_posts()) : while(have_posts()) : the_post(); ?>
	<div class="plans col-sm-6 col-md-4 teaser reveal">
		<a href="<?php the_permalink(); ?>">link</a>
		<div class="inner match-h">
			<?php the_post_thumbnail('medium-landscape'); ?>
	 		<div class="details">
	 			<h4 ><?php the_title(); ?></h4>
		        <?php if( get_field('bedrooms')): ?>
		          <span class="bedrooms"><?php the_field('bedrooms'); ?></span>
		        <?php endif; ?>
		        <?php if( get_field('bathrooms')): ?>
		          <span class="bathrooms"><?php the_field('bathrooms'); ?></span>
		        <?php endif; ?>
		        <?php if( get_field('car_spaces')): ?>
		          <span class="car-spaces"><?php the_field('car_spaces'); ?></span>
		        <?php endif; ?>
	      	</div>
 		</div>
 	</div>
<?php endwhile; endif;?>


