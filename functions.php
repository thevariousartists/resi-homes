<?php
/**
 * Sage includes
 *
 * The $sage_includes array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 *
 * Please note that missing files will produce a fatal error.
 *
 * @link https://github.com/roots/sage/pull/1042
 */
$sage_includes = [
  'functions.php',
  'lib/assets.php',    // Scripts and stylesheets
  'lib/extras.php',    // Custom functions
  'lib/setup.php',     // Theme setup
  'lib/wrapper.php',   // Theme wrapper class
  'lib/customizer.php', // Theme customizer
  'lib/bs4navwalker.php', // BS4 Nav Walker  
  'lib/titles.php', // BS4 Nav Walker
  'lib/va.php'          // VA stuff
];



foreach ($sage_includes as $file) {
  if (!$filepath = locate_template($file)) {
    trigger_error(sprintf(__('Error locating %s for inclusion', 'sage'), $file), E_USER_ERROR);
  }

  require_once $filepath;
}
unset($file, $filepath);


// ACF Google Maps API

function my_acf_google_map_api( $api ){
  
  $api['key'] = 'AIzaSyAYnF8zzXvsLjgU7uSHHsRwkVuFMACs1T0';
  
  return $api;
  
}
add_filter('acf/fields/google_map/api', 'my_acf_google_map_api');


// Gravity Forms Jquery in Footer
add_filter( 'gform_init_scripts_footer', '__return_true' );


// register top menu
function register_top_menu() {
  register_nav_menu('top-menu',__( 'Top Menu' ));
}
add_action( 'init', 'register_top_menu' );

// register footer menu
function register_footer_menu() {
  register_nav_menu('footer-menu',__( 'Footer Menu' ));
}
add_action( 'init', 'register_footer_menu' );


// new widget area FEATURED
function feature_widgets_init() {
  register_sidebar( array(
    'name'          => 'Featured',
    'id'            => 'featured',
    'before_widget' => '<div>',
    'after_widget'  => '</div>',
    'before_title'  => '<h2>',
    'after_title'   => '</h2>',
  ) );
}
add_action( 'widgets_init', 'feature_widgets_init' );

// new widget area ABOVE FOOTER
function above_footer_widgets_init() {
  register_sidebar( array(
    'name'          => 'Above footer',
    'id'            => 'above-footer',
    'before_widget' => '<div>',
    'after_widget'  => '</div>',
    'before_title'  => '<h2>',
    'after_title'   => '</h2>',
  ) );
}
add_action( 'widgets_init', 'above_footer_widgets_init' );

// 
add_theme_support( 'post-thumbnails' );
// Add featured image sizes

//add_image_size( 'custom', 2000, 100, true );
add_image_size( 'medium-landscape', 380, 280, array( 'center', 'center') ); // 
add_image_size( 'medium-square', 400, 400, array( 'center', 'center') ); // 
add_image_size( 'large-landscape', 900, 450, array( 'center', 'center') ); // 
add_image_size( 'x-large-landscape', 1200, 600, array( 'center', 'center') ); // 
add_image_size( 'small-strip', 500, 200, array( 'center', 'center') ); // 
add_image_size( 'medium-strip', 900, 275, array( 'center', 'center') ); // 
add_image_size( 'large-strip', 1200, 350, array( 'center', 'center') ); // 
add_image_size( 'x-large-strip', 1800, 400, array( 'center', 'center') ); //

// Add not-home to body class
function add_not_home_body_class($classes) {
    if( !is_front_page() ) $classes[] = 'not-home';
    return $classes;
}
add_filter('body_class','add_not_home_body_class');

