
<div class="col-12">
<h1 class="page-title"><?php esc_html_e( 'Oops! That page can&rsquo;t be found.', 'basefour' ); ?></h1>

<p>It looks like nothing was found at this location. Go to the <a href="/">home page</a>, <a href="/wp-login">login</a> or search using the form below:</p>
</div>
<div class="col-12 py-5">
<?php
get_search_form();

?>
</div>

					