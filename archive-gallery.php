<h1 class="col-12">Our Builds</h1>


<?php if(have_posts()) : while(have_posts()) : the_post(); ?>
	<div class="gallery col-sm-6 col-md-4 teaser reveal">
		<a href="<?php the_permalink(); ?>">link</a>
		<div class="inner match-h ">
			<?php the_post_thumbnail('medium-landscape'); ?>
			 <div class="details details-build text-center">
	 			<h4 ><?php the_title(); ?></h4>
	      	</div>
 		</div> 
 	</div>

<?php endwhile; endif;?>