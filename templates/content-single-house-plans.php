<?php while (have_posts()) : the_post(); ?>
<article <?php post_class(); ?>>
  <div class="entry-summary container">
  	<div class="row">
      <a href="/house-plans" class="btn btn-primary view-all">View all plans</a>
      <h1 class="col-12"><?php the_title(); ?></h1>

    <?php 

    //the_excerpt(); ?>

    <div class="col-md-8 left-section">
    	<?php the_post_thumbnail('large'); ?>

      <div class="details">
        <?php if( get_field('bedrooms')): ?>
          <span class="bedrooms"><?php the_field('bedrooms'); ?></span>
        <?php endif; ?>
        <?php if( get_field('bathrooms')): ?>
          <span class="bathrooms"><?php the_field('bathrooms'); ?></span>
        <?php endif; ?>
        <?php if( get_field('car_spaces')): ?>
          <span class="car-spaces"><?php the_field('car_spaces'); ?></span>
        <?php endif; ?>
      </div>

      <div class="py-4">
        <?php the_content(); ?>
      </div>

      

    <div class="plans-gallery col-12">
      <?php the_field('gallery'); ?>
    </div>

    </div>
    <div class="col-md-4 right-section">

        <?php if( get_field('features')): ?>
          <div class="features">
            <h3>Features</h3>
            <?php the_field('features'); ?>    
          </div>
        <?php endif; ?>

        <?php if( get_field('areas')): ?>
          <div class="areas">
            <h3>Areas</h3>
            <?php the_field('areas'); ?>    
          </div>
        <?php endif; ?>

    <?php $file = get_field('download');
		if( $file ): ?>
			<a href="<?php echo $file['url']; ?>" target="_blank">Download PDF</a>
		<?php endif; ?>
    </div>    




	</div>
  </div>
</article>

<?php endwhile; ?>