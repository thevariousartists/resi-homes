<?php if ( !is_front_page() )  : // hide on home ?>

  <?php if( get_field('column_1') OR get_field('column_2') OR get_field('column_3')  ): ?>
  <div class="columns-sections relative pad-y">
    <div class="container">

      <?php if ( is_page( 3481 ) ) { // process - show sections as accordian ?>

      <div id="accordion">

        <h2 >Standard Incusions</h2>

        <h3>PREMIUM INCLUSIONS & ADDED VALUE</h3>

         <div class="card">
            <div class="card-header" id="headingOne">
              <h5 class="mb-0">
                <button class="btn " data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                  General Inclusions
                </button>
              </h5>
            </div>

            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
              <div class="card-body">
                <?php the_field('column_1'); ?>
              </div>
            </div>
          </div>
          <div class="card">
            <div class="card-header" id="headingTwo">
              <h5 class="mb-0">
                <button class="btn collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                  External Inclusions
                </button>
              </h5>
            </div>
            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
              <div class="card-body">
                <?php the_field('column_2'); ?>
              </div>
            </div>
          </div>
          <div class="card">
            <div class="card-header" id="headingThree">
              <h5 class="mb-0">
                <button class="btn collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                  Internal Inclusions
                </button>
              </h5>
            </div>
            <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
              <div class="card-body">
                <?php the_field('column_3'); ?>
            </div>
          </div>
        </div>

      </div>
      <?php } else { ?>

        <div class="row">
          <div class="col-md-4 col1"> 
            <?php the_field('column_1'); ?>
          </div>
          <div class="col-md-4 col2"> 
            <?php the_field('column_2'); ?>
          </div>
          <div class="col-md-4 col3"> 
            <?php the_field('column_3'); ?>
          </div> 
        </div>
    
      <?php } endif; ?>

    </div>
  </div>

  <?php if ( is_page( 4403  ) ) { // show feat builds on contact, process-3481 ?>
  <div class="featured layout"> 
    <div class="container"> 
      <div class="row justify-content-center">
        <div class="col-md-10 text-center pad-y">
          <?php dynamic_sidebar( 'featured' ); ?>
        </div>
      </div>
    </div>
  </div>
  <?php } ?>

  <?php if ( is_page( 4403 ) ||  is_page( 3481 ) ) { // show feat builds on contact, process-3481 ?>
  <div class="builds  pad-y">  
    <div class="title-wrapper px-3">
      <h2>INTERESTED IN OUR READY-MADE PLANS?</h2>
    </div>
    <?php print get_template_part('templates/blocks/house-plan-teasers'); ?>
  </div>
  <?php } ?>

  <?php if ( is_page( 15 ) ) { // show feat builds on contact, process ?>
  <div class="builds  pad-y">  
    <div class="title-wrapper px-3">
      <h2>Our Builds</h2>
      <h3>PRESTIGE BUILDS COME TO LIFE</h3>
    </div>
    <?php print get_template_part('templates/blocks/builds-teasers'); ?>
  </div>
  <?php } ?>

  <?php if ( is_active_sidebar( 'above footer' ) ) { ?>
  <div class="above-footer layout"> 
    <div class="container"> 
      <div class="row justify-content-center">
        <div class="col-md-10 text-center  py-5">
          <?php dynamic_sidebar( 'above footer' ); ?>
        </div>
      </div>
    </div>
  </div>
  <?php } ?>

<?php endif;  // /hide on home ?>




