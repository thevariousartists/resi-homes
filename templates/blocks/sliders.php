
<?php 

  $the_query = new WP_Query(
    array(
      'post_type'    => 'sliders',
      'post_status'  => 'publish', 
      // 'meta_key'     => 'month',
      //'orderby'      => 'meta_value',
      'order'        => 'ASC',
      //'posts_per_page'       => '4',
      // 'meta_query' => array(
      //     array(
      //           'key'   => 'start_date',
      //           'compare' => '>=',
      //           'value'   => $today,
      //       ),
      //   ),
    )
  );

  ?>


<div id="home-carousel" class="carousel slide" >

  <div class="carousel-inner " role="listbox">

    <?php
      // The Loop
      $k = 0;
        while ( $the_query->have_posts() ) : $the_query->the_post();
      $k++;
    ?>

    <div id="slide-<?php echo $k; ?>" class="carousel-item <?php if ($k == 1) { echo 'active '; } ?>">
      
      <div class="slide-img">
         
        <?php
          // vars
          $markers = get_field('markers');  
          if( $markers['marker_1_title'] ): ?>
            <div class="marker marker-1">
              <i></i>
              <div>
              <h4><?php echo $markers['marker_1_title'];  ?></h4>
              <p><?php echo $markers['marker_1_text'];  ?> </p>
              </div>
            </div>
          <?php endif; ?>
          <?php if( $markers['marker_2_title'] ): ?>
            <div class="marker marker-2 ">
              <i></i>
              <div>
              <h4><?php echo $markers['marker_2_title'];  ?></h4>
              <p><?php echo $markers['marker_2_text'];  ?></p>
              </div>
            </div>
        <?php endif; ?>


        <div class="slide-text">
            <article>
                <h2><?php the_title(); ?></h2>
                <?php the_content(); ?>
            </article>     
        </div>
        
          <!-- use background image for different sizes as per media query -->
          <style type="text/css">
            @media (max-width : 767px){ #slide-<?php echo $k; ?> .slide-img span.bg-image{ background: url(<?php the_post_thumbnail_url('medium'); ?>)}}
            @media (min-width : 768px){ #slide-<?php echo $k; ?> .slide-img span.bg-image{ background:url(<?php the_post_thumbnail_url('medium'); ?>)}}
            @media (min-width : 922px){ #slide-<?php echo $k; ?> .slide-img span.bg-image{ background:url(<?php the_post_thumbnail_url('large'); ?>)}}
          </style>
          
          <span class="bg-image"></span>

        </div>

        

    </div>

    <?php
      endwhile;
      wp_reset_postdata();
      wp_reset_query(); 
    ?> 
  </div>

      <ol class="carousel-indicators">
          <?php
          // The Loop
            $i = -1;
            while ( $the_query->have_posts() ) : $the_query->the_post();
            $i++;
          ?>
          <li data-target="#home-carousel" data-slide-to="<?php echo $i; ?>" class="<?php if ($i == 0) { echo 'active'; } ?>"></li>
        <?php
         endwhile;
          wp_reset_postdata();
          wp_reset_query(); 
        ?>
      </ol>

</div>