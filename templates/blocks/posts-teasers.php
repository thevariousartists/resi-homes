<?php $myposts = get_posts( 'numberposts=-1&offset=$debut' );

      foreach( $myposts as $post) : setup_postdata( $post ) ?>
		
		<article <?php post_class('post-teaser row'); ?>>


			<div class="col-sm-4">
				<?php the_post_thumbnail( 'medium-landscape' ); ?>
			</div>

			<div class="col-sm-8">
	          <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>

	          <!-- Only display part of the post so the user has to click "More!" -->
	          <?php the_excerpt(); ?>

	          <a href="<?php the_permalink(); ?>" class="btn btn-primary">Read more</a>

			</div>

        </article>

<?php endforeach; ?>