
<?php 

  $the_query = new WP_Query(
    array(
      'post_type'    => 'gallery',
      'post_status'  => 'publish', 
      // 'meta_key'     => 'month',
      //'orderby'      => 'meta_value',
      'order'        => 'DESC',
      'posts_per_page'       => '-1',
      // 'meta_query' => array(
      //     array(
      //           'key'   => 'start_date',
      //           'compare' => '>=',
      //           'value'   => $today,
      //       ),
      //   ),
    )
  );

  ?>

  <div class="row">
    <?php
    // The Loop
    $i = 0;
    while ( $the_query->have_posts() ) : $the_query->the_post();
    ?>

  <?php 
    $thumbnail_url = get_the_post_thumbnail_url();
  ?>

  <article class="gallery reveal col-sm-3 teaser">
    
        <div class="entry-summary">
          <a href="<?php the_permalink(); ?>">Link</a>
          <?php the_post_thumbnail( 'medium' ); ?>
          <?php //the_excerpt(); ?>
          <p><strong><?php the_title(); ?></strong></p>
        </div>

    </article>
  <?php
  	endwhile;

    wp_reset_postdata();
    wp_reset_query(); 

  ?>

  </div>


