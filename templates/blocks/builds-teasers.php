
<?php 

  $the_query = new WP_Query(
    array(
      'post_type'    => 'gallery',
      'post_status'  => 'publish', 
      // 'meta_key'     => 'month',
      //'orderby'      => 'meta_value',
      'order'        => 'DESC',
      'posts_per_page'       => '4',
      // 'meta_query' => array(
      //     array(
      //           'key'   => 'start_date',
      //           'compare' => '>=',
      //           'value'   => $today,
      //       ),
      //   ),
    )
  );

  ?>

<div class="container">
  <div class="row">
    <?php
    // The Loop
    $i = 0;
    while ( $the_query->have_posts() ) : $the_query->the_post();
    ?>

  <?php 
    $thumbnail_url = get_the_post_thumbnail_url();
  ?>

  <article class="reveal col-sm-6 col-md-3 teaser">
    
        <div class="entry-summary">
          <a href="<?php the_permalink(); ?>">Link</a>
          <?php the_post_thumbnail( 'medium-landscape' ); ?>
          <?php //the_excerpt(); ?>
          <p ><strong><?php the_title(); ?></strong></p>
          
        </div>

    </article>
  <?php
  	endwhile;

    wp_reset_postdata();
    wp_reset_query(); 

  ?>

  </div>
  <a href="/builds" class="btn btn-primary mt-2">View all builds</a>
</div>

