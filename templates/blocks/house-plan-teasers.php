
<?php 

  $the_query = new WP_Query(
    array(
      'post_type'    => 'house-plans',
      'post_status'  => 'publish', 
      // 'meta_key'     => 'month',
      //'orderby'      => 'meta_value',
      'order'        => 'DESC',
      'posts_per_page'       => '-1',
      // 'meta_query' => array(
      //     array(
      //           'key'   => 'start_date',
      //           'compare' => '>=',
      //           'value'   => $today,
      //       ),
      //   ),
    )
  );

  ?>

 <div class="container">
  <div class="row">
    <?php
    // The Loop
    $i = 0;
    while ( $the_query->have_posts() ) : $the_query->the_post();
    ?>

  <?php 
    $thumbnail_url = get_the_post_thumbnail_url();
  ?>

  <article class=" reveal col-6 col-sm-3 teaser">
    
        <div class="entry-summary">
          <a href="<?php the_permalink(); ?>">Link</a>
          <?php the_post_thumbnail( 'medium' ); ?>
          <?php //the_excerpt(); ?>
          <p><strong><?php the_title(); ?></strong></p>
        </div>

    </article>
 
    <?php
    	endwhile;
      wp_reset_postdata();
      wp_reset_query(); 
    ?>

  </div>

</div> 


<!-- /////


<div class="container text-center my-3">
    <div class="row mx-auto my-auto">
        <div id="myCarousel" class="carousel slide w-100" data-ride="carousel">
            <div class="carousel-inner" role="listbox">

            <?php
                // Item size (set here the number of posts for each group)
                $i = 4; 

                // Set the arguments for the query
                global $post; 
                $args = array( 
                  'numberposts'   => -1, // -1 is for all
                  'post_type'     => 'house-plans', // or 'post', 'page'
                  'orderby'       => 'title', // or 'date', 'rand'
                  'order'         => 'ASC', // or 'DESC'
                );

                // Get the posts
                $myposts = get_posts($args);

                // If there are posts
                if($myposts):

                  // Groups the posts in groups of $i
                  $chunks = array_chunk($myposts, $i);
                  $html = "";

                  /*
                   * Item
                   * For each group (chunk) it generates an item
                   */
                  foreach($chunks as $chunk):
                    // Sets as 'active' the first item
                    ($chunk === reset($chunks)) ? $active = "active" : $active = "";
                    $html .= '<div class="carousel-item  '.$active.'"><div class="container"><div class="row">';

                    /*
                     * Posts inside the current Item
                     * For each item it generates the posts HTML
                     */
                    foreach($chunk as $post):
                      $html .= '<div class="col-sm-3">';
                      $html .= '<img src="';
                      $html .= get_the_post_thumbnail_url();
                      $html .= '">';
                      $html .= get_the_title($post->ID);
                      $html .= '</div>';
                    endforeach;

                    $html .= '</div></div></div>';  

                  endforeach;

                  // Prints the HTML
                  echo $html;

                endif;
                ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <btn class="carousel-control-prev text-dark" href="#myCarousel" role="button" data-slide="prev">
                <span class="btn btn-primary">Previous</span>
            </btn>
            <btn class="carousel-control-next text-dark " href="#myCarousel" role="button" data-slide="next">
                <span class="btn btn-primary">Next</span>
            </btn>
        </div>
    </div>
</div>
 -->