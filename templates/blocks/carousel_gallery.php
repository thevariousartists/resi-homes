
<?php 

  $the_query = new WP_Query(
    array(
      'post_type'    => 'page',
      'post_status'  => 'publish', 
      // 'meta_key'     => 'month',
      //'orderby'      => 'meta_value',
      'order'        => 'ASC',
      //'posts_per_page'       => '4',
      // 'meta_query' => array(
      //     array(
      //           'key'   => 'start_date',
      //           'compare' => '>=',
      //           'value'   => $today,
      //       ),
      //   ),
    )
  );

  ?>

<?php 

$images = get_field('image_gallery');
$size = 'medium'; // (thumbnail, medium, large, full or custom size)
$count=0;
$count1=0;

if ( is_page( 4403 ) && ( $images )) : ?>
<div id="carousel-gallery" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
      <?php foreach( $images as $image ): ?>
        <li data-target="#carousel-gallery" data-slide-to="<?php echo $count; ?>" <?php if($count==0) : ?>class="active"<?php endif; ?>></li>      
      <?php $count++; endforeach; ?>
  </ol>

  <div class="carousel-inner" role="listbox">
    <?php foreach( $images as $image ): ?>
            <div class="carousel-item <?php if($count1==0) : echo ' active'; endif; ?>">
                <?php echo wp_get_attachment_image( $image['ID'], $size ); ?>    
            </div><!-- item -->
    <?php  $count1++; endforeach; ?>  

  </div>
</div>

<?php else: ?>

    <?php foreach( $images as $image ): ?>
      <?php echo wp_get_attachment_image( $image['ID'], $size ); ?>    
    <?php endforeach; ?>  

</div>

<?php endif; ?>

