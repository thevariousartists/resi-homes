
<footer id="footer" class="content-info layout-footer footer">
  <div class="container">

  	<div class="row">
  		<div class="footer-col col-lg-3">
  			<h2>Contact Resi Homes</h2>
          <div class="address">
             <?php the_field('contact_address', 15); ?>
          </div> 
	        <div class="icon call">
              <a href="tel:<?php the_field('contact_phone', 15); ?>">
                <?php the_field('contact_phone', 15); ?>
              </a>
            </div>
            <span class="icon houzz">
              <a href="<?php the_field('contact_houzz_link', 15); ?>" target="_blank">
                <?php the_field('contact_houzz_link', 15); ?>
              </a>
            </span>
            <span class="icon fb">
              <a href="<?php the_field('contact_facebook_link', 15); ?>" target="_blank">
                <?php the_field('contact_facebook_link', 15); ?>
            </a>
          </span> 
    	</div>
  		<div class="footer-col col-lg-3">
  			<h2>Resi Homes</h2>
  			<p>Whether you are building your home for the first time, looking to expand or settle, or you are an experienced investor, RESI HOMES is the builder for you.</p>

			<p>Working in conjunction with <a href="http://www.seapointehomes.com.au/" target="_blank">Seapointe Homes.</a></p>
    	</div> 
  		<div class="footer-col col-lg-3">
  			<h2>Quick links</h2>
  			<?php wp_nav_menu( array( 'theme_location' => 'footer-menu' ) ); ?>
    	</div> 	
  		<div class="footer-col logo col-lg-3">
  			<a href="<?= esc_url(home_url('/')); ?>"><img src="<?php print get_template_directory_uri();?>/logo-white.png" /><span class='sr-only'><?php bloginfo('name'); ?></span></a> 
    	</div> 	
    </div>             


  </div>
</footer>
<div class="layout-subfooter text-center">
	<div class="container subfooter">
	  	<div class="row">
		    
		      <div class="col-md-12"><p><span class="copyright">© Copyright <?php echo date('Y');?> <?php bloginfo('name'); ?></span><span class="sep"> | </span><span class="va">Website by <a href="http://www.weareva.com.au" target="_blank">VA.</a></span></p>
		      </div>
		    
	    </div>
	</div>
</div>