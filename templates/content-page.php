<?php if ( is_page( 4403 ) ): // custom plans - show carousel gallery  ?>

	<div class="row"> 
		<div class="col-md-7"> 
			<?php the_content(); ?>
		</div>
		<div class="col-md-5 py-4"> 
		    <?php print get_template_part('templates/blocks/carousel_gallery'); ?>
		</div>
	</div>

<?php else: ?>

	<?php the_content(); ?>

<?php endif; ?>


 
<?php if ( is_page( array( 3481, 4933 ) ) ) { // process - show button ?>

	<?php if( have_rows('process_repeater') ): // REPEATER ?>
		<div id="accordion-process" role="tablist">
			<?php $i=1; while ( have_rows('process_repeater') ) : the_row(); ?>
				<div class="card">
				    <div class="card-header" role="tab" id="heading-<?php echo $i; ?>">
				      <div class="mb-0"> 
				        <button class="btn btn-link" data-toggle="collapse" data-target="#collapse_<?php echo $i; ?>" aria-expanded="<?php if ($i==1) { echo 'true'; } ?>" aria-controls="collapse_1">
				          <h5><?php the_sub_field('process_title'); ?></h5>
				          <h4><?php the_sub_field('process_subtitle'); ?></h4>
				        </button>
				      </div>
				    </div>
				    <div id="collapse_<?php echo $i; ?>" class="collapse <?php if ($i==1) { echo 'show'; } ?>" role="tabpanel" data-parent="#accordion" aria-labelledby="heading-<?php echo $i; ?>">
				      <div class="card-body row">
				      	<div class="col-md-6">
				       		<?php the_sub_field('process_text'); ?>	
				       	</div>
				       	<div class="col-md-6">
							<img src="<?php $image = get_sub_field('process_image'); print($image['sizes']['medium']); ?>" alt="<?php echo $image['alt']; ?>" />
						</div>
				      </div>
				    </div>
				</div>
			<?php $i++; endwhile; ?>
		</div>
	<?php endif; ?>

	<h4 style="text-align: center;">Have a question about the process?</h4>
	<p style="text-align: center;"><a class="btn btn-primary" href="/contact">Contact Resi Homes</a></p>

<?php } ?>

<?php if ( is_page( 3489 ) ) { // FAQ ?>

	<?php if( have_rows('faq_repeater') ): // REPEATER ?>
		<div id="accordion" role="tablist">
			<?php $i=1; while ( have_rows('faq_repeater') ) : the_row(); ?>
				<div class="card">
				    <div class="card-header" role="tab" id="heading-<?php echo $i; ?>">
				      <div class="mb-0"> 
				        <button class="btn btn-link" data-toggle="collapse" data-target="#collapse_<?php echo $i; ?>" aria-expanded="<?php if ($i==1) { echo 'true'; } ?>" aria-controls="collapseOne">
				          <h4><?php the_sub_field('faq_title'); ?></h4 >
				        </button>
				      </div>
				    </div>
				    <div id="collapse_<?php echo $i; ?>" class="collapse <?php if ($i==1) { echo 'show'; } ?>" role="tabpanel" data-parent="#accordion" aria-labelledby="heading-<?php echo $i; ?>">
				      <div class="card-body ">
				       	<?php the_sub_field('faq_text'); ?>	
				      </div>
				    </div>
				</div>
			<?php $i++; endwhile; ?>
		</div>
	<?php endif; ?>
	
<?php } ?>



<?php if ( is_page( 4783 ) ) { // show articles teasers on articles page ?>

  <div class="post-teasers page-4783-content-page">
      <?php print get_template_part('templates/blocks/posts-teasers'); ?>
  </div>
<?php } ?>



<?php wp_link_pages(['before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']); ?>