
<?php while (have_posts()) : the_post(); ?>
<article <?php post_class(); ?>>
  <div class="col">
  <div class="entry-summary container">
  	<div class="row">
      <a href="/builds" class="btn btn-primary view-all">View all builds</a>
    <?php $image = get_field('portrait');
      if( !empty($image) ): ?>
        <span class="ppl-portrait"><img src="<?php $image = get_field('portrait'); print($image['sizes']['medium-square']); ?> "></span>
    <?php endif; ?>

    <div class="page-header col-md-12">

      <h4><?php the_field('name'); ?></h4>
      <h1><?php the_title(); ?></h1>

    </div>
    <div class="col-md-9 left-section">
      
      <?php if( get_field('location') ): ?>
        <span class="location">
          <img src="<?php echo get_template_directory_uri(); ?>/dist/images/map-marker-point.svg"> 
          <?php the_field('location'); ?>
        </span>
      <?php endif; ?>
      <?php if( get_field('date') ): ?>
        <span class="date">
          <img src="<?php echo get_template_directory_uri(); ?>/dist/images/calendar.svg"> 
          <?php the_field('date'); ?>
        </span>
      <?php endif; ?>

      <div class="py-4">
    	<?php the_content(); ?>
      </div>

  

    </div>    
    <div class="col-md-3 right-section">
        <h3>Gallery</h3>
        <?php the_field('gallery'); ?> 
    </div>  

     <?php 
      // Relationships - reference field
      $posts = get_field('reference');
      if( $posts ): ?>
        <div class="col-sm-12 reference-section pad-y mt-5">

        <h3 class="text-center">You may also be interested in...</h3>
          <div class="builds row">
          <?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
              <?php setup_postdata($post); ?>

               <article class="reveal col-sm-6 col-md-3 teaser">
    
                  <div class="entry-summary">
                    <a href="<?php the_permalink(); ?>">Link</a>
                    <?php the_post_thumbnail( 'medium-landscape' ); ?>
                    <?php //the_excerpt(); ?>
                    <p><strong><?php the_title(); ?></strong></p>
                  </div>

              </article>

          <?php endforeach; ?>
          </ul>
          <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>

        </div>

      <?php endif; ?>

	</div>
  </div>
</div>
</article>

<?php endwhile; ?>