<?php dynamic_sidebar('sidebar-primary'); ?>



	  	<h2>Contact</h2>
		<p><strong>Phone: </strong><br> <a href="tel:+<?php the_field('contact_phone', 15); ?>">
            <?php the_field('contact_phone', 15); ?>
        </a></p>
		<p><?php the_field('contact_address', 15); ?></p>
		<p><?php the_field('contact_working_hours', 15); ?></p>
		<p><strong><?php the_field('contact_name', 15); ?></strong><br><?php the_field('contact_licence', 15); ?></p>
		<p><?php the_field('contact_text', 15); ?></p>
