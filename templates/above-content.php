


<?php if ( is_front_page() )  :?>
<div class="home-banner banner-img">
  <style type="text/css">
    @media (max-width : 767px){ .banner-img{ background-image:url(<?php the_post_thumbnail_url('medium-landscape'); ?>)}}
    @media (min-width : 768px){ .banner-img{ background-image:url(<?php the_post_thumbnail_url('large-landscape'); ?>)}}
    @media (min-width : 922px){ .banner-img{ background-image:url(<?php the_post_thumbnail_url('x-large-landscape'); ?>)}}
/*    @media (min-width : 1200px){ .banner-img{ background:url(<?php the_post_thumbnail_url('large-landscape'); ?>)}}   */       
  </style>

  <div class="banner-text"> 
    <h1><?php the_title(); ?></h1>
    <?php while (have_posts()) : the_post(); ?>
     <?php the_content(); ?>
    <?php endwhile; ?>

  </div>
  <a href="#anchor-link" class="anchor"><svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
   viewBox="0 0 31.479 31.479" style="enable-background:new 0 0 31.479 31.479;" xml:space="preserve">
<path d="M26.485,21.206c0.429-0.444,0.429-1.143,0-1.587c-0.444-0.429-1.159-0.429-1.587,0l-8.047,8.047
  V1.111C16.851,0.492,16.359,0,15.74,0c-0.619,0-1.127,0.492-1.127,1.111v26.555l-8.031-8.047c-0.444-0.429-1.143-0.429-1.587,0
  c-0.429,0.444-0.429,1.143,0,1.587l9.952,9.952c0.429,0.429,1.143,0.429,1.587,0L26.485,21.206z"/>
</svg></a> 
</div>

  <div id="anchor-link"></div>
  <?php // show slider
    print get_template_part('templates/blocks/sliders'); 
  ?>

  

  <?php // banner on home - custom builds
    if ( is_front_page() )  :?>
    <div class="custom-banner" > 
      <div class="pad-y container"> 
        <div class="row"> 
          <div class="content col-12"> 
            <?php the_field('banner_text'); ?>
          </div>
        </div>
      </div>
    </div>
  <?php endif ?>



  <div id="featured" class="featured pad-y">  
    <?php dynamic_sidebar( 'featured' ); ?>
  </div>
  
    <?php else: // not FRONT page below ?>

      <?php if( get_field('intro') && has_post_thumbnail() ): // intro field and image half/half ?>

          <!-- use background image for different sizes as per media query -->
          <style type="text/css">
            @media (max-width : 767px){ .feat-img{ background:url(<?php the_post_thumbnail_url(medium); ?>)}}
            @media (min-width : 768px){ .feat-img{ background:url(<?php the_post_thumbnail_url(medium); ?>)}}
            @media (min-width : 922px){ .feat-img{ background:url(<?php the_post_thumbnail_url(large); ?>)}}
          </style>

        <div class="above-content split">
          <div class="feat-img match-h">&nbsp;</div>  
          <div class="container"> 

            <div class="row"> 
              <div class="intro match-h content col-md-6">
                <div class="pad-y reveal">
                  <?php the_field('intro'); ?>
                </div>
              </div>
            </div>
            
          </div>
        </div>

      <?php endif ?>

    <?php endif; ?>
  
  <?php if (is_singular('post') ): //  ?>
    
      <!-- use background image for different sizes as per media query -->
      <style type="text/css">
        @media (max-width : 767px){ .feat-img{ background:url(<?php the_post_thumbnail_url('small-strip'); ?>)}}
        @media (min-width : 768px){ .feat-img{ background:url(<?php the_post_thumbnail_url('medium-strip'); ?>)}}
        @media (min-width : 922px){ .feat-img{ background:url(<?php the_post_thumbnail_url('large-strip'); ?>)}}
        @media (min-width : 1200px){ .feat-img{ background:url(<?php the_post_thumbnail_url('x-large-strip'); ?>)}}
      </style>

    <div class="above-content split">  
      <div class="feat-img match-h">&nbsp;</div>

       <div class="container"> 

            <div class="row"> 
              <div class="intro match-h content col-md-6">
                <div class="pad-y reveal">
                   <?php the_title( '<h1>', '</h1>' ); ?>
                   <span class="categories"><?php the_category(' ') ?></span>
                </div>
              </div>
            </div>
            
          </div>
    </div>

   <?php endif ?>

  
   <?php if (is_singular('gallery') ): //  ?>
    
      <!-- use background image for different sizes as per media query -->
      <style type="text/css">
        @media (max-width : 767px){ .feat-img{ background:url(<?php the_post_thumbnail_url('small-strip'); ?>)}}
        @media (min-width : 768px){ .feat-img{ background:url(<?php the_post_thumbnail_url('medium-strip'); ?>)}}
        @media (min-width : 922px){ .feat-img{ background:url(<?php the_post_thumbnail_url('large-strip'); ?>)}}
        @media (min-width : 1200px){ .feat-img{ background:url(<?php the_post_thumbnail_url('x-large-strip'); ?>)}}
      </style>

    <div class="above-content">  
      <div class="feat-img">&nbsp;</div>
    </div>

   <?php endif ?>


