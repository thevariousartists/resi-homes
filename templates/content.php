
<div class="col">

<article <?php post_class('post-teaser row'); ?>>


			<div class="col-sm-4">
				<?php the_post_thumbnail( 'medium-landscape' ); ?>
			</div>

			<div class="col-sm-8">
	          <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>

	          <!-- Only display part of the post so the user has to click "More!" -->
	          <?php the_excerpt(); ?>

	          <a href="<?php the_permalink(); ?>" class="btn btn-primary">Read more</a>

			</div>

</article>

<?php 
	/*
	*  Query posts for a relationship value.
	*  This method uses the meta_query LIKE to match the string "123" to the database value a:1:{i:0;s:3:"123";} (serialized array)
	*/

	$revrels = get_posts(array(
		'post_type' => 'gallery',
		'meta_query' => array(
			array(
				'key' => 'reference', // name of custom field
				'value' => '"' . get_the_ID() . '"', // matches exactly "123", not just 123. This prevents a match for "1234"
				'compare' => 'LIKE'
			)
		)
	));

	?>
	<?php if( $revrels ): ?>
		
		<div class="col-sm-12 reference-section pad-y mt-5">

		<h3>You may also be interested in...</h3>

		<div class="builds row">

		<?php foreach( $revrels as $revrel ): ?>


			<article class="reveal col-sm-4 teaser">
    
                  <div class="entry-summary">
                    <a href="<?php echo get_permalink( $revrel->ID ); ?>">Link</a>
                    <?php echo get_the_post_thumbnail( $revrel->ID, 'medium-landscape' ); ?>
                    <?php //the_excerpt(); ?>
                    <p><strong><?php echo get_the_title( $revrel->ID ); ?></strong></p>
                  </div>

            </article>

		<?php endforeach; ?>
		
		</div>

		</div>

	<?php endif; ?>

</div>