<header class="header <?php echo !is_front_page() ? '' : '';?> fixed-top " id="header">

  <nav id="navbar<?php echo is_front_page() ? '-home' : '';?>" class="<?php echo is_front_page() ? ' is-home' : ' not-home';?>"> 

    <div class="container navbar-expand-lg">       

      <div class="row">

        <a class="col-4 col-md-2 logo" href="<?= esc_url(home_url('/')); ?>"><img src="<?php print get_template_directory_uri();?>/logo.png" /><span class='sr-only'><?php bloginfo('name'); ?></span></a> 

        <div class="col-8 col-md-10 order-1 order-sm-2 text-right float-right align-self-start">

          <div class="row row-contact">
            <div class="col-11 col-lg-12"> 
              <span class="icon call">
                <a href="tel:+<?php the_field('contact_phone', 15); ?>">
                  <?php the_field('contact_phone', 15); ?>
                </a>
              </span>
              <span class="icon houzz">
                <a href="<?php the_field('contact_houzz_link', 15); ?>" target="_blank">
                  <?php the_field('contact_houzz_link', 15); ?>
                </a>
              </span>
              <span class="icon fb">
                <a href="<?php the_field('contact_facebook_link', 15); ?>" target="_blank">
                  <?php the_field('contact_facebook_link', 15); ?>
                </a>
              </span>              

              <?php wp_nav_menu( array( 'theme_location' => 'top-menu' ) ); ?>
            </div><!-- /col -->
          </div><!-- /row -->

          <div class="row row-menu">
            <div id="navbarLayout" class="layout-nav collapse navbar-collapse order-4 col-1 col-lg-12 relative">

            <?php
             if (has_nav_menu('primary_navigation')) :
               wp_nav_menu([
                 'menu'            => 'primary_navigation',
                 'theme_location'  => 'primary_navigation',
                 'container'       => false,//'div',
                 'menu_id'         => false,
                 'menu_class'      => 'navbar-nav ml-auto justify-content-lg-end',
                 'depth'           => 2,
                 'fallback_cb'     => 'bs4navwalker::fallback',
                 'walker'          => new bs4navwalker()
               ]); 
             endif;
            ?>
            </div><!-- /navbarLayout -->
          </div>
          <button id="navbar-toggler" class="collapsed navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarLayout" aria-controls="navbarLayout" aria-expanded="false" aria-label="Toggle navigation">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div><!-- /row -->

      </div><!-- /row -->

    </div><!-- /container -->
    
  </nav>
</header>

<div id="header-placeholder"></div><!-- /container -->
