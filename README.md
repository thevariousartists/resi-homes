# [RocketSage]
[![Build Status](https://travis-ci.org/roots/sage.svg)](https://travis-ci.org/roots/sage)
[![devDependency Status](https://david-dm.org/roots/sage/dev-status.svg)](https://david-dm.org/roots/sage#info=devDependencies)

**RocketSage is a VA WordPress starter theme based on Sage.**
Sage is a WordPress starter theme based on HTML5 Boilerplate, gulp, Bower, and Bootstrap Sass, that will help you make better themes.

## Installation

Make sure [Composer](https://getcomposer.org/download/) has been installed before moving on.

1. Install RocketSage by copying the project into a new folder within your WordPress themes directory. Copy the theme from the Surfaris Retreat guinea-pig site for now :)

2. Navigate to your Kalabox project environment. In Terminal, switch to the /code directory of your project. e.g. `cd myproject/code`

3. Run the VA setup shell script to install a few basic plugins and Soil (plugin from Roots/Sage) `sh va-setup.sh`

## Features

### Customisable Bootstrap index file
Usually Sage builds Bootstrap automatically with every feature included whether you like it or not. However, this bloats the CSS, so like our Drupal builds, now you have assets/styles/bootstrap/_bootstrap.scss that you can change and comment out components you don't use for a project (like the carousel, which we never use).

### matchHeight
We use the _matchHeight_ JavaScript plugin in most projects at VA, so it's automatically included. You can uncomment this in the `assets/manifest.json` file.

### Boostrappy nav/menu markup
There is an extra 'nav walker' function in so the nav menu mirrors a more Bootstrap code/style. (In Drupal, the theme comes with a similar thing built in).

### Bootstrap 3 styled hamburger icon
The BS4 hamburger icon (aka navbar toggler) has been ripped out and replaced it with the BS3 one. BS4 has an SVG in place, but it's clunky to style and to change the colours (in Skye's opinion). 
I liked the old style so we could animate the three lines into an 'X' when you click it.

### Custom sidebar regions
There are a bunch of custom sidebars added into the `lib/setup.php` `widgets_init()` function that are similar to the Drupal regions we have (e.g. Top, Canyon, Below Content, Footer). To disable these for your project just comment these out!

## Theme setup

Edit `lib/setup.php` to enable or disable theme features, setup navigation menus, post thumbnail sizes, post formats, and sidebars.

### Install gulp and Bower

Kalabox comes with gulp and Bower installed, so you run 'kbox' before any bower, npm or gulp commands.

It may be faster to run & compile gulp locally. I wouldn't bother with local bower/npm because they tend to be run once per project). 

_(these are the original Sage instructions below): _
From the command line:
1. Install [gulp](http://gulpjs.com) and [Bower](http://bower.io/) globally with `npm install -g gulp bower`
2. Navigate to the theme directory, then run `npm install`
3. Run `bower install`

You now have all the necessary dependencies to run the build process.

### Available gulp commands

* `gulp` — Compile and optimize the files in your assets directory
* `gulp watch` — Compile assets when file changes are made
* `gulp --production` — Compile assets for production (no source maps).

### Using BrowserSync

To use BrowserSync during `gulp watch` you need to update `devUrl` at the bottom of `assets/manifest.json` to reflect your local development hostname.

For example, if your local development URL is `http://project-name.dev` you would update the file to read:
```json
...
  "config": {
    "devUrl": "http://project-name.dev"
  }
...
```
If your local development URL looks like `http://localhost:8888/project-name/` you would update the file to read:
```json
...
  "config": {
    "devUrl": "http://localhost:8888/project-name/"
  }
...
```

## Documentation

Sage documentation is available at [https://roots.io/sage/docs/](https://roots.io/sage/docs/).
