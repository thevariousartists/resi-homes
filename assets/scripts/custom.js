/* Implement custom javascript here */
(function ($, window, document, undefined) {
  'use strict';

  $(function () {

	//alert("Hello! I am an alert box!! custom/js");

	window.sr = ScrollReveal();
	sr.reveal('.reveal');

	// set Matchheight class to be used in template
	$('.match-h').matchHeight();
		
	//slider hover dots show text
	// $(".slide-img .marker").hover(function(){
	//     $(this).children().hide();
	// });

	$( ".slide-img .marker" ).click(function() {
	 // $( this ).toggleClass("show");
	  $(this).toggleClass('show').siblings().removeClass('show');
	});

	// if ($('.navbar-toggler').hasClass('.collapsed')) {
	//     $( "header" ).addClass('clicked');
	// }

	$(".navbar-toggler").click(function(){
	    $("body").toggleClass("no-scroll");
	});

	});

	$(window).scroll(function(){
	  var sticky = $('.header'),
	      scroll = $(window).scrollTop();

	  if (scroll >= 100) {
	  	sticky.addClass('scrolled');
	  	//$('#header_placeholder').css({display: 'block'});
	  } else {
	  	sticky.removeClass('scrolled');
	  	//$('#header_placeholder').css({display: 'none'});
	  }
	  var scroll = $(window).scrollTop();
	  var scrollcenter = (scroll+50);
	  $(".banner-img").css({
           "background-position-y": (scroll/5)  + "%",
           //"-webkit-filter": "blur(" + (scroll/125) + "px)",
           //filter: "blur(" + (scroll/125) + "px)"
      });


	});

    //})

}(jQuery, window, window.document));

