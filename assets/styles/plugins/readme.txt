Plugins is where we put styles and overrides for WordPress contributed and custom plugins.
 * e.g. "Slick" slider styles could go in a _slick.scss file