Base is where we place 'base' global styles for HTML elements and also:
  * SASS Mixins
  * SASS Placeholders
  * HTML Element styles (h1, h2, lists etc)
  * Print styles
  * Typography styles and CSS helper classes. Responsive typography
  * Extra global variables
