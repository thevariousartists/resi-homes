<?php
/**
 * Template Name: Page with sidebar contact
 */
?>

<?php while (have_posts()) : the_post(); ?>
	<section class="col-md-8 left-section">
	  	<?php get_template_part('templates/page', 'header'); ?>
	  	<?php get_template_part('templates/content', 'page'); ?>
	</section>
	<aside class="col-md-4 right-section py-3">
	  	<h2>Contact</h2>
		<p><strong>Phone: </strong><br> <a href="tel:+<?php the_field('contact_phone', 15); ?>">
            <?php the_field('contact_phone', 15); ?>
        </a></p>
		<p><?php the_field('contact_address', 15); ?></p>
		<p><?php the_field('contact_working_hours', 15); ?></p>
		<p><strong><?php the_field('contact_name', 15); ?></strong><br><?php the_field('contact_licence', 15); ?></p>
		<p><?php the_field('contact_text', 15); ?></p>
	</aside>
<?php endwhile; ?>


<?php if( get_field('column_1') OR get_field('column_2') OR get_field('column_3')  ): ?>
<div class="container left-section">
	<div class="row">
		<div class="col-md-4 col1">	
			<?php the_field('column_1'); ?>
		</div>
		<div class="col-md-4 col2">	
			<?php the_field('column_2'); ?>
		</div>
		<div class="col-md-4 col3">	
			<?php the_field('column_3'); ?>
		</div> 
  	</div>
</div>
<?php endif; ?>