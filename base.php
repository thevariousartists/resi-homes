<?php

use Roots\Sage\Setup;
use Roots\Sage\Wrapper;

?>

<!doctype html>
<html <?php language_attributes(); ?>>
  <?php get_template_part('templates/head'); ?>
  <body <?php body_class(); ?>>

    <?php if ( function_exists( 'gtm4wp_the_gtm_tag' ) ) { gtm4wp_the_gtm_tag(); } ?>
    
    <!--[if IE]>
      <div class="alert alert-warning">
        <?php _e('You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.', 'sage'); ?>
      </div>
    <![endif]-->
    <?php
      do_action('get_header');
      get_template_part('templates/header');
    ?>

    <?php
      do_action('get_above_content');
      get_template_part('templates/above-content');
    ?>

    <?php if ( is_front_page() ) { ?>
    <div class="wrap page-wrapper" role="document">
      <div class="content">
        <main class="main">
          <?php include Wrapper\template_path(); ?>
        </main><!-- /.main -->
      </div><!-- /.content -->
    </div><!-- /.wrap -->
    <?php } else { ?>
    
    <div class="wrap page-wrapper" role="document">
      <div class="content container pad-y">
        <main class="main row">
          <?php include Wrapper\template_path(); ?>

          <?php if (Setup\display_sidebar()) : ?>
            <aside class="col-md-4 right-section py-3">
              <?php include Wrapper\sidebar_path(); ?>
            </aside><!-- /.sidebar -->
          <?php endif; ?>

        </main><!-- /.main -->
        
      </div><!-- /.content -->
    </div><!-- /.wrap --> 
    <?php } ?>

    <?php
      do_action('get_below_content');
      get_template_part('templates/below-content');
    ?>

    <?php
      do_action('get_footer');
      get_template_part('templates/footer');
      wp_footer();
    ?>
  </body>
</html>
