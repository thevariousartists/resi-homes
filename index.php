<?php //get_template_part('templates/page', 'header'); ?>


	<?php if (!have_posts()) : ?>
	  <div class="alert alert-warning">
	    <?php _e('Sorry, no results were found.', 'sage'); ?>
	  </div>
	  <?php get_search_form(); ?>
	<?php endif; ?>

	<div class="post-list">
		<?php
		if ( is_category() ) {
			echo '<div class="text-center">';
 				the_archive_title( '<h1 class="page-title">', '</h1>' ); 
 			echo '</div>';
 		} 
 		?>

	  <?php while (have_posts()) : the_post(); ?>
	    <?php get_template_part('templates/content', get_post_type() != 'post' ? get_post_type() : get_post_format()); ?>
	  <?php endwhile; ?>
	</div>

<?php the_posts_navigation(); ?>
