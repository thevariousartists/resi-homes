<?php
/**
 * Template Name: Home Template
 */
?>

<div class="builds pad-y">  
    <div class="title-wrapper px-3">
      <h2>Our Builds</h2>
      <h3>PRESTIGE BUILDS COME TO LIFE</h3>
    </div>
	<?php print get_template_part('templates/blocks/builds-teasers'); ?>
</div>

<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/page', 'header'); ?>
	<div class="main-content pad-y">  
		<div class="container columns-sections">
			<div class="row">
				<div class="col-md-12">
					<h2>OUR DIFFERENCE</h2>
					<h3>WHY PEOPLE LOVE RESI HOMES</h3>
		  		</div>

		  		<?php if( get_field('column_1') OR get_field('column_2') OR get_field('column_3')  ): ?>
		  		<div class="col-lg-4 col1">	
		  			<?php the_field('column_1'); ?>
		  		</div>
		   		<div class="col-lg-4 col2">	
		  			<?php the_field('column_2'); ?>
		  		</div>
		   		<div class="col-lg-4 col3">	
		  			<?php the_field('column_3'); ?>
		  		</div> 
		  		<?php endif; ?>
		  
		  <?php wp_link_pages(['before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']); ?>
		  </div>
		</div>
	</div>
<?php endwhile; ?>

<style type="text/css">
	@media (max-width : 767px){ .feat-img{ background:url(<?php $image = get_field('vision_image'); print($image['sizes']['medium']); ?>); ?>)}}
	@media (min-width : 768px){ .feat-img{ background:url(<?php $image = get_field('vision_image'); print($image['sizes']['large']); ?>)}}
</style>

<div class="relative split">  
	<div class="feat-img match-h">&nbsp;</div>  
	<div class="container"> 
		<div class="row"> 
			<div class="intro match-h content col-md-6 ml-auto ">
				<div class="pad-y reveal ">
					<?php the_field('vision_text'); ?>
				</div>
			</div>
		</div>
	</div>
</div>

        

